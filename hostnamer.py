#!/usr/bin/env python

adjectives = [
'1bit',     'hostile',  'naked',       'short',      'grumpy',        'illegal',
'2bit',     'frienldy', 'smiling',     'enchanted',  'enthuisiastic', 'forbidden',
'3bit',     'angry',    'smoking',     'old',        'sleepy',        'melting',
'8bit',     'pink',     'frozen',      'new',        'hungover',      'maniac',
'16bit',    'purple',   'walking',     'nice',       'genius',        'dangerous',
'32bit',    'strong',   'secret',      'unfriendly', 'lonely',        'explosive',
'64bit',    'annoying', 'hidden',      'fine',       'mad',           'innocent',
'large',    'drunken',  'enormous',    'excellent',  'crazy',         'guilty',
'great',    'sober',    'tempting',    'wrong',      'awesome',       'virtual',
'big',      'holy',     'magic',       'talking',    'miniature',     'flat',
'small',    'sleeping', 'sweet',       'real',       'mega',          'double',
'tiny',     'singing',  'curious',     'false',      'stoned',        'triple',
'yellow',   'dancing',  'good',        'plastic',    'black',         'single',
'blue',     'eating',   'bad',         'invisible',  'white',         'sad',
'green',    'dry',      'odd',         'burning',    'wise',          'decaying',
'red',      'hungry',   'long',        'happy',      'powerful',      'rusty',

'quadruple','shiny',    'radioactive', 'reactive',   'cool',          'busy',
'boring',   'bored',    'thirsty',     'fast',       'slow',          'medium',
'rare',     'common',   'regular',     'local',      'legal',         'humble',
'golden',   'arrogant', 'ignorant',    'stinky',     'mental',        'octo',
'quad',     'half',     'third',       'transparent','hyper',         'dizzy',
'foggy',    'war',      'empty',       'full',       'solid',         'chiral',
'n-fold',   'symmetric','stupendous',  'amazing',    'fifth',         'militant',
'remarkable','phenomenal','astounding','staggering',
]

names = [
'apple',      'pear',     'astroid', 'sonic',     'worker',      'neutron', 
'banana',     'orange',   'martian', 'eggman',    'skateboard',  'muon',
'avocado',    'melon',    'android', 'egg',       'mushroom',    'tau',
'grapefruit', 'zombie',   'robot',   'turtle',    'meteroid',    'lepton',
'dog',        'cat',      'fish',    'tortoise',  'alien',       'hadron',
'rabbit',     'monkey',   'squid',   'dolphin',   'planet',      'meson',
'ape',        'bird',     'stone',   'unicorn',   'star',        'baryon',
'zero',       'hippo',    'tree',    'fridge',    'sun',         'quark',
'car',        'screw',    'flower',  'mountain',  'glacier',     'photon',
'mario',      'bucket',   'liquid',  'leopard',   'geysir',      'gluon',
'luigi',      'barrel',   'water',   'tiger',     'cave',        'tachyon',
'island',     'rock',     'beer',    'lion',      'pool',        'graviton',
'spaceship',  'orc',      'milk',    'wolf',      'wave',        'boson',
'bicycle',    'elf',      'wine',    'lake',      'nucleus',     'fermion',
'lizard',     'mastodon', 'potato',  'sea',       'electron',    'neutrino',
'spider',     'elephant', 'pizza',   'scientist', 'proton',      'positron',

'salami',     'burger',   'pasta',   'salad',     'soup',        'sausage',
'snail',      'bat',      'kitten',  'puppy',     'cell',        'butterfly',
'neuron',     'axon',     'axion',   'phage',     'membrane',    'ball',
'sphere',     'pyramid',  'cube',    'monster',   'olive',       'tomato',
'onion',      'phonon',   'genome',  'molecule',  'diamond',     'forest',
'digit',      'library',  'book',    'bug',       'rose',        'lotus',
'treasure',   'worm',     'wizard',  'witch',     'fruit',       'volcano',
'lemming',    'phone',    'ion',     'sneaker',   'hat',         'key',
'spaghetti',  'lasagne',  'panther', 'poison',    'capricorn',   'bull',
'goat',       'archer',   'scorpion','crab',      'shrimp',      'twin',
'moon',       'snake',    'horse',   'octahedron','triangle',    'polyhedron',
'vertex',     'pixel',    'voxel',   'tetahedron','prism',       'supernova',
'singularity','squash',   'eggplant','pacman',    'wario',


]

import random
mac="7c:c7:09:%02x:%02x:%02x" % (random.randint(0,255),random.randint(0,255),random.randint(0,255))

print mac

s=mac.split(':')

adj_idx = int(s[3]+s[4][0],16)
name_idx = int(s[4][1]+s[5],16)

print adj_idx,name_idx

hostname = adjectives[ adj_idx % len(adjectives) ] + '-' + names[ name_idx % len(names) ]

print hostname
